#!/bin/bash

# To reset the transmitter:
# (1) Turn off the transmitter if it is on.
echo " Make sure the transmitter is turned off. "
read -p "Press any key to continue..."

# (2) Make sure that the steering trim knob is at the center position
echo " Make sure that the steering trim knob is at the center position. "
read -p "Press any key to continue..."

# (3) Turn the G.SPD L (speed limiter, right side) knob fully to the counterclockwise direction.
# (4) Position of G.SPD H (LED control, left side) knob does not matter.
echo " Turn the G.SPD L (speed limiter, right side) knob fully to the counterclockwise direction. "
echo " (Position of G.SPD H (LED control, left side) knob does not matter.) "
read -p "Press any key to continue..."

# (5) Turn the steering wheel to the fully right position and push the throttle away to the full-brake position and hold them.
rostopic pub -1 /control rc_msgs/ControlSignal '{delta: 3200.0 ,power: 0.0}' 
# (6) Turn on the transmitter. The LED should flash rapidly; this means you are now in the factory mode.
echo " Turn on the transmitter. The LED should flash rapidly; this means you are now in the factory mode. "
read -p "Press any key to continue..."
# (7) Release the steering wheel and throttle lever.
rostopic pub -1 /control rc_msgs/ControlSignal '{delta: 1600.0 ,power: 1600.0}' 
# (8) Turn the steering wheel fully to the left, right, left and right, and then release it.
echo " Calibrating steering. Please wait."
rostopic pub -1 /control rc_msgs/ControlSignal '{delta: 0.0 ,power: 1600.0}' 
rostopic pub -1 /control rc_msgs/ControlSignal '{delta: 3200.0 ,power: 1600.0}' 
rostopic pub -1 /control rc_msgs/ControlSignal '{delta: 0.0 ,power: 1600.0}' 
rostopic pub -1 /control rc_msgs/ControlSignal '{delta: 3200.0 ,power: 1600.0}' 
rostopic pub -1 /control rc_msgs/ControlSignal '{delta: 1600.0 ,power: 1600.0}' 
# (9) Pull/push the throttle lever to full-throttle, full-brake, full-throttle, and full-brake, and then release it.
echo " Calibrating throttle. Please wait. " 
rostopic pub -1 /control rc_msgs/ControlSignal '{delta: 1600.0 ,power: 3200.0}' 
rostopic pub -1 /control rc_msgs/ControlSignal '{delta: 1600.0 ,power: 0.0}' 
rostopic pub -1 /control rc_msgs/ControlSignal '{delta: 1600.0 ,power: 3200.0}' 
rostopic pub -1 /control rc_msgs/ControlSignal '{delta: 1600.0 ,power: 0.0}'
rostopic pub -1 /control rc_msgs/ControlSignal '{delta: 1600.0 ,power: 1600.0}'  
# (10) Turn the G.SPD L knob to the right most position. The LED should become solid.
echo " Turn the G.SPD L knob to the right most position. The LED should become solid. " 
read -p "Press any key to continue..."
# (11) Turn off the transmitter and on it again. You are done.
echo " Turn off the transmitter and on it again. You are done. " 
